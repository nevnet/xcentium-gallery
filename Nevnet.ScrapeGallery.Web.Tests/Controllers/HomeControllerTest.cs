﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nevnet.ScrapeGallery.Web;
using Nevnet.ScrapeGallery.Web.Controllers;

namespace Nevnet.ScrapeGallery.Web.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void HomeControllerReturnsDefaultView()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("XCentium Gallery Sample", result.ViewBag.Title);
        }
    }
}
