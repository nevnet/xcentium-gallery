﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nevnet.ScrapeGallery.Web;
using Nevnet.ScrapeGallery.Web.Controllers;
using Nevnet.ScrapeGallery.Services;
using System.Net.Http;
using Nevnet.ScrapeGallery.Tokenize;
using System.Web.Http;
using System.Web.Http.Results;
using Nevnet.ScrapeGallery.Web.Models;
using System;

namespace Nevnet.ScrapeGallery.Web.Tests.Controllers
{
    [TestClass]
    public class ScrapeControllerTest
    {
        [TestMethod]
        public void ValidUrlReturnsOk()
        {
            // Arrange
            var httpClient = new HttpClient();
            var tokenizer = new DefaultEnglishTokenizer();
            var service = new ScrapeService(httpClient, tokenizer);
            ScrapeController controller = new ScrapeController(service);

            // Act
            var result = controller.Scrape("http://www.google.com").Result as OkNegotiatedContentResult<ImageGalleryModel>;
            
            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Content.WordCount > 0);
        }

        [TestMethod]
        public void InvalidUrlReturnsBadRequest()
        {
            // Arrange
            var httpClient = new HttpClient();
            var tokenizer = new DefaultEnglishTokenizer();
            var service = new ScrapeService(httpClient, tokenizer);
            ScrapeController controller = new ScrapeController(service);

            // Act
            var result = controller.Scrape("http://www.google").Result as BadRequestErrorMessageResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
