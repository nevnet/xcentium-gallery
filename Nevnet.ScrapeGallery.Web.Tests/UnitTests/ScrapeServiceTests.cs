﻿using HtmlAgilityPack;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nevnet.ScrapeGallery.Services;
using Nevnet.ScrapeGallery.Tokenize;
using System;
using System.Linq;
using System.Net.Http;

namespace Nevnet.ScrapeGallery.Web.Tests.UnitTests
{
    [TestClass]
    public class ScrapeServiceTests
    {
        [TestMethod]
        public void GoogleIntegrationTest()
        {
            var httpClient = new HttpClient();
            var tokenizer = new DefaultEnglishTokenizer();
            var service = new ScrapeService(httpClient, tokenizer);

            var result = service.Scrape(new Uri("http://www.google.com")).Result;

            Assert.IsNotNull(result);
            Assert.IsTrue(result.TotalWords > 0);
        }

        [TestMethod]
        public void ScrapeUsingContextToBypassHttpClient()
        {
            var httpClient = new HttpClient();
            var tokenizer = new DefaultEnglishTokenizer();
            var service = new ScrapeService(httpClient, tokenizer);

            var context = ScrapeContext.Create(new Uri("http://server.com"));
            context.Document = new HtmlDocument();
            context.Document.LoadHtml("<html><body><p>John eats apples<p>John eat apple</p></p></p></p>John likes apples<b>John doesn't eat apples</b><img src=\"findme.jpg\"/> one... two.. three. four-4 five_555<img src=\"data:dontfindme\"/><img src=\"dontfindme.svg\" /></body></html>");

            var result = service.Scrape(context);

            Assert.IsNotNull(result);
            Assert.AreEqual(18, result.TotalWords);
            Assert.AreEqual(10, result.Top10Words.Length);
            Assert.AreEqual("john", result.Top10Words[0].Item1);
            Assert.AreEqual(4, result.Top10Words[0].Item2);
            Assert.AreEqual(1, result.Images.Length);
            Assert.AreEqual("http://server.com/findme.jpg", result.Images[0]);
        }

        [TestMethod]
        public void DefaultTokenizerReturnsCorrectWordCount()
        {
            var tokenizer = new DefaultEnglishTokenizer();
            var tokens = tokenizer.Tokenize("hello, my name is john smith and I'm always eating apples !!");

            Assert.IsNotNull(tokens);
            Assert.AreEqual(9, tokens.Count());
        }

        [TestMethod]
        public void SimpleTokenizerReturnsCorrectWordCount()
        {
            var tokenizer = new SimpleTokenizer();
            var tokens = tokenizer.Tokenize("hello, my name is john smith and I'm always eating apples !!");

            Assert.IsNotNull(tokens);
            Assert.AreEqual(12, tokens.Count());
        }
    }
}
