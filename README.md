# README #

This is a submission for the XCentium code test challenge.

The requirement was to allow a user to enter a URL and from that page display:

* Images in a carousel or gallery
* The total number of words
* The top 10 words and their count in a table or chart

My solution was to supply a simple SPA and allow the back-end to do the heavy lifting. I have avoided using a complicated architecture as my assumption is the reviewer would prefer to open the solution and hit F5 to see the result. I have also stripped out a lot of the cruft from the default VS templates to focus on the core solution, that being a Web API controller calling the Scrape service to retrieve and display the result.

[View online version](http://gallery.nevnetinteractive.com)

### Release notes (1.0.0) ###

This is a combined ASP.NET MCV/Web API project. It targets .NET 4.5.2 and uses the new Roslyn compiler so Visual Studio 2015 is required to build the project.

It is a single page application that uses Angular & Angular-UI (bootstrap) and should look great on all devices.

Has been tested on iPhone, Android 5.2, iPad, Chrome, Firefox, IE 10 and new the Windows 10 'Edge' browser.

The HTML Agility Pack is used as the HTML parser.

Lucene.NET is used to analyze and tokenize the parsed HTML.

Unit and integration tests are included, however they are limited to important public methods (and one internal method).

### Product backlog ###

* Add logging and global exception handler
* Add support for SVG and raw data image
* Support crawling CSS for additional image
* Modify ScrapeResult to carry a success/error status instead of throwing exceptions
* Improve styling of page (including scaling the images in the carousel nicely & displaying alt text)
* Test on additional browsers