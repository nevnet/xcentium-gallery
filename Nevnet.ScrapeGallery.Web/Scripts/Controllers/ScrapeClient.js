﻿var ScrapeApp = angular.module('ScrapeApp', ['ui.bootstrap']);

ScrapeApp.controller('ScrapeController', ['$scope', 'ScrapeService', function ($scope, ScrapeService) {

    $scope.defaultUrl = 'http://edition.cnn.com/sport';
    $scope.showResults = false;
    $scope.progress = false;
    $scope.hasError = false;
    $scope.hideReadme = true;

    $scope.getPage = function (url) {
        $scope.progress = true;
        $scope.showResults = false;
        $scope.hasError = false;
        $scope.images = null;

        ScrapeService.getPage(url)
        .success(function (result) {
            $scope.images = result.Images;
            $scope.wordCount = result.WordCount;
            $scope.top10Words = result.Top10Words;
            $scope.timeElapsed = result.TimeElapsed;
            $scope.showResults = true;
            $scope.hasError = false;
            $scope.progress = false;
            console.log(result);
        })
        .error(function (error) {
            $scope.status = 'Sorry, there was a problem fetching that page: ' + error.Message;
            $scope.hasError = true;
            $scope.progress = false;
            console.log(error);
        });
    }
}]);

ScrapeApp.factory('ScrapeService', ['$http', function ($http) {
    var ScrapeService = {};
    ScrapeService.getPage = function (url) {
        return $http.get('/api/Scrape?url=' + url);
    };
    return ScrapeService;
}]);