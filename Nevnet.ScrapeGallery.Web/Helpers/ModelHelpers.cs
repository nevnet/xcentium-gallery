﻿using Nevnet.ScrapeGallery.Services;
using Nevnet.ScrapeGallery.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nevnet.ScrapeGallery.Web.Helpers
{
    public static class ModelHelpers
    {
        /// <summary>
        /// Maps a ScrapeResult service DTO to an ImageGalleryModel model
        /// </summary>
        /// <param name="result"></param>
        /// <returns>ImageGalleryModel</returns>
        public static ImageGalleryModel ToImageGalleryModel(this ScrapeResult result)
        {
            return new ImageGalleryModel()
            {
                TimeElapsed = Math.Round(result.TimeElapsed.TotalSeconds, 6),
                Images = result.Images,
                Top10Words = result.Top10Words,
                WordCount = result.TotalWords
            };
        }
    }
}