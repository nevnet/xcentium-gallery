﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Nevnet.ScrapeGallery.Services;
using Nevnet.ScrapeGallery.Tokenize;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;


namespace Nevnet.ScrapeGallery.Web.Utility
{
    /// <summary>
    /// Castle container bootstrap class used to wire up the entire application object graph.
    /// </summary>
    public class WindsorInstaller : IWindsorInstaller
    {
        public WindsorInstaller() { }

        /// <summary>
        /// Castle container bootstrap entry point
        /// </summary>
        /// <param name="container">An instance of a Castle container</param>
        /// <param name="store">An instance of a Castle configuration store</param>
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var httpClient = new HttpClient();

            // Adding the user agent string gets us past a few bot-blockers
            httpClient.DefaultRequestHeaders.Add("user-agent", "Mozilla/5.0 (compatible; MSIE 10.0;)");


            container.Register(
                // Register all our web api controllers, scoped per request
                Classes.FromThisAssembly().BasedOn<ApiController>().LifestyleScoped(),
                Component.For<ICanTokenize>().ImplementedBy<DefaultEnglishTokenizer>().LifestyleTransient(),
                Component.For<HttpClient>().Instance(httpClient),
                Component.For<IScrapeService>().ImplementedBy<ScrapeService>().LifestyleSingleton()
            );
        }
    }
}