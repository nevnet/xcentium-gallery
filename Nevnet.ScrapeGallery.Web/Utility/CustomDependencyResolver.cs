﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Dependencies;
using Castle.Windsor;
using Castle.MicroKernel.Lifestyle;

namespace Nevnet.ScrapeGallery.Web.Utility
{
    /// <summary>
    /// This resolver replaces the default Web API/MVC dependency resolver and allows us to use
    /// Castle instead. The main purpose is to allow API/MCV controllers to utilise paramterized constructors.
    /// </summary>
    public class CustomDependencyResolver : IDependencyResolver
    {
        private IWindsorContainer container;

        public CustomDependencyResolver(IWindsorContainer container) 
        {
            this.container = container;
        }

        public IDependencyScope BeginScope()
        {
            return new CustomDependencyScope(container);
        }

        public object GetService(Type serviceType)
        {
            return container.Kernel.HasComponent(serviceType) ? container.Resolve(serviceType) : null;
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return container.ResolveAll(serviceType).Cast<object>();
        }

        public void Dispose()
        {
        }
    }

    public class CustomDependencyScope : IDependencyScope
    {
        private IWindsorContainer container;
        private IDisposable disposable;

        public CustomDependencyScope(IWindsorContainer container)
        {
            this.container = container;
            disposable = container.Kernel.BeginScope();
        }

        public object GetService(Type serviceType)
        {
            return container.Kernel.HasComponent(serviceType) ? container.Resolve(serviceType) : null;
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return container.ResolveAll(serviceType).Cast<object>();
        }

        public void Dispose()
        {
            disposable.Dispose();
        }
    }
}