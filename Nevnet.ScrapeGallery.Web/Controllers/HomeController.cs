﻿using System.Web.Mvc;

namespace Nevnet.ScrapeGallery.Web.Controllers
{
    /// <summary>
    /// Nothing to do here except return the default index view.
    /// The app interacts using Web API, so check out ScrapeController for the fun stuff.
    /// </summary>
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "XCentium Gallery Sample";

            return View();
        }
    }
}
