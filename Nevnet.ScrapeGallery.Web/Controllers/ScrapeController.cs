﻿using Nevnet.ScrapeGallery.Services;
using Nevnet.ScrapeGallery.Web.Helpers;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Nevnet.ScrapeGallery.Web.Controllers
{
    /// <summary>
    /// The app only has one Web API method called Scrape that accepts a URL string and returns
    /// a result object with all te required data in it for the view.
    /// </summary>
    public class ScrapeController : ApiController
    {
        private IScrapeService service;
        //private ILog log;

        /// <summary>
        /// The controller construcer gets injected with a concrete service object at runtime (See WindsorInstaller.cs)
        /// </summary>
        /// <param name="service">An implementation of IScrapeService that can return a ScrapeResult.</param>
        public ScrapeController(IScrapeService service)
        {
            this.service = service;
            //this.log = log;
        }

        /// <summary>
        /// This method retrieves the results of a page scraping operation.
        /// The method is Async as the scraping service uses HttpClient internally.
        /// </summary>
        /// <param name="url">The URL for the site to be processed.</param>
        /// <remarks>Note: The supplied URL must be in absolute format.</remarks>
        /// <returns>IHttpActionResult</returns>
        [HttpGet]
        public async Task<IHttpActionResult> Scrape(string url)
        {
            // Validate to ensure only absolute URLs are supplied
            Uri uri = null;
            if(!Uri.TryCreate(url, UriKind.Absolute, out uri))
            {
                return BadRequest("URL is not valid. Ensure you specify 'http://'.");
            }

            try
            {
                // Call scrape service
                var result = await service.Scrape(uri);

                return Ok(result.ToImageGalleryModel());
            }
            catch (UriFormatException)
            {
                // Url may be absolute, but it can still be unacceptable for HttpClient
                return BadRequest("URL is not valid.");
            }
            catch (HttpRequestException ex)
            {
                // This exception has UI friendly error messages so OK to show it
                return BadRequest((ex.InnerException ?? ex).Message);
            }
            catch (Exception ex)
            {
                // Would usually let this one flow through to a global exception handler
                // log.Error(ex);
                return BadRequest("An unexpected error has occurred. Support ID - " + Request.GetCorrelationId().ToString("D"));
            }
        }
    }
}
