﻿using Castle.Windsor;
using Nevnet.ScrapeGallery.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Nevnet.ScrapeGallery.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Use Castle for the wire-up in place of the default resolver
            var container = new WindsorContainer();
            container.Install(new WindsorInstaller());
            config.DependencyResolver = new CustomDependencyResolver(container);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
