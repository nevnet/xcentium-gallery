﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nevnet.ScrapeGallery.Web.Models
{
    public class ImageGalleryModel
    {
        public double TimeElapsed { get; set; }

        public IEnumerable<string> Images { get; set; }

        public IEnumerable<Tuple<string, int>> Top10Words { get; set; }

        public int WordCount { get; set; }

        public ImageGalleryModel() { }
    }
}
