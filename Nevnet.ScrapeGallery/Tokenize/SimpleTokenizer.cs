﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nevnet.ScrapeGallery.Tokenize
{
    public class SimpleTokenizer : ICanTokenize
    {
        public SimpleTokenizer() { }

        public IEnumerable<string> Tokenize(string input)
        {
            return input.Split(new char[] { ' ', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
