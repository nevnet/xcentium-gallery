﻿using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Analysis.Tokenattributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nevnet.ScrapeGallery.Tokenize
{
    public class DefaultEnglishTokenizer : ICanTokenize
    {
        public DefaultEnglishTokenizer() { }

        public IEnumerable<string> Tokenize(string input)
        {
            var tokens = new List<string>();
            using (var analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30))
            //using (var analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30, new HashSet<string>())) // Use this one to exclude 'and', 'the', 'is', etc
            using (var tokenStream = analyzer.TokenStream(null, new StringReader(input)))
            {
                while (tokenStream.IncrementToken())
                {
                    var termAttribute = tokenStream.GetAttribute<ITermAttribute>();
                    //var typeAttribute = tokenStream.GetAttribute<ITypeAttribute>();
                    //if (typeAttribute.Type == "<ALPHANUM>")
                    tokens.Add(termAttribute.Term);
                }
                tokenStream.End();
            }

            return tokens;
        }
    }
}
