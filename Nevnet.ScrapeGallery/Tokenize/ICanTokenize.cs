﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nevnet.ScrapeGallery.Tokenize
{
    public interface ICanTokenize
    {
        IEnumerable<string> Tokenize(string input);
    }
}
