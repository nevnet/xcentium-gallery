﻿using System;
using System.Threading.Tasks;

namespace Nevnet.ScrapeGallery.Services
{
    public interface IScrapeService
    {
        Task<ScrapeResult> Scrape(Uri target);
    }
}