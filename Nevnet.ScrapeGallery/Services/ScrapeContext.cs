﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nevnet.ScrapeGallery.Services
{
    /// <summary>
    /// Internal class used to carry data during a scrape operation.
    /// </summary>
    public class ScrapeContext
    {
        public Uri Target { get; set; }
        public DateTime Start { get; set; }
        public HtmlDocument Document { get; set; }
        public List<string> Images { get; set; }
        public List<string> TokenizedChunks { get; set; }
        public int WordCount { get; set; }

        private ScrapeContext() {
            Images = new List<string>();
            TokenizedChunks = new List<string>();
        }

        internal static ScrapeContext Create(Uri target)
        {
            return new ScrapeContext() {
                Target = target,
                Start = DateTime.UtcNow
            };
        }
    }
}
