﻿using HtmlAgilityPack;
using Nevnet.ScrapeGallery.Tokenize;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Nevnet.ScrapeGallery.Services
{
    public class ScrapeService : IScrapeService
    {
        private ICanTokenize tokenizer;
        private HttpClient httpClient;

        /// <summary>
        /// This service is responsible for coordinating the actions of the HttpClient, HTML parser and Tokeniser.
        /// The total word count and the word ranking logic currently both use the same tokenizer so english words
        /// such as 'and', 'this', 'a', and 'or' will not count towards the total number of words or appear in rankings.
        /// </summary>
        /// <param name="httpClient">Only one HttpClient instance required per application, so pass it in as a dependency.</param>
        /// <param name="tokenizer">The tokenizer strategy to use to determine word count and ranking.</param>
        public ScrapeService(HttpClient httpClient, ICanTokenize tokenizer)
        {
            this.httpClient = httpClient;
            this.tokenizer = tokenizer;
        }

        public async Task<ScrapeResult> Scrape(Uri target)
        {
            if (target == null) throw new UriFormatException();

            var scrapeContext = ScrapeContext.Create(target);

            // Download the HTML content and add document to scrape context
            await LoadHtmlDocument(scrapeContext);

            return Scrape(scrapeContext);
        }

        internal ScrapeResult Scrape(ScrapeContext context)
        {
            // Parse HTML document and process relevant nodes of interest (img & text)
            var bodyElement = context.Document.DocumentNode.Element("html").Element("body");
            foreach (var element in bodyElement.Descendants())
            {
                if (element.Name == "img")
                    HandleImage(element, context);

                if (element.Name == "#text")
                    HandleText(element, tokenizer, context);
            }

            return new ScrapeResult()
            {
                TimeElapsed = DateTime.UtcNow.Subtract(context.Start),
                TotalWords = context.WordCount,
                Top10Words = CalculateTopWords(10, context.TokenizedChunks),
                Images = context.Images.ToArray()
            };
        }

        private Tuple<string, int>[] CalculateTopWords(int top, IEnumerable<string> tokenizedChunks)
        {
            return tokenizedChunks
                .GroupBy(word => word)
                .Select(group => new Tuple<string, int>(group.Key, group.Count()))
                .Where(group => group.Item2 > 0)
                .OrderByDescending(group => group.Item2)
                .Take(top)
                .ToArray();
        }

        private void HandleImage(HtmlNode element, ScrapeContext context)
        {
            var attribute = element.Attributes["src"];
            if (attribute != null && !string.IsNullOrWhiteSpace(attribute.Value) && !context.Images.Contains(attribute.Value))
            {
                // Ignore raw data images, there seem to be a few in use these days
                if (attribute.Value.StartsWith("data:", StringComparison.OrdinalIgnoreCase))
                    return;

                // Ignore .svg images, they don't render in the carousel
                if (attribute.Value.EndsWith(".svg", StringComparison.OrdinalIgnoreCase))
                    return;

                // Need to re-base image URLs to their host if they are defined in relative format
                Uri src = null;
                if (Uri.TryCreate(attribute.Value, UriKind.RelativeOrAbsolute, out src))
                {
                    if(src.IsAbsoluteUri)
                    {
                        context.Images.Add(src.AbsoluteUri);
                    }
                    else
                    {
                        context.Images.Add(new Uri(context.Target, src).AbsoluteUri);
                    }
                }
            }
        }

        private void HandleText(HtmlNode element, ICanTokenize tokenizer, ScrapeContext context)
        {
            var textElement = element as HtmlTextNode;
            if (textElement == null)
                return;

            // Ignore script elements, they only contain junk
            if (string.IsNullOrWhiteSpace(textElement.Text) || textElement.ParentNode.Name == "script" || textElement.ParentNode.Name == "style")
                return;

            // Tokenize the text element and update the total word count
            var tokens = tokenizer.Tokenize(WebUtility.HtmlDecode(textElement.Text));
            context.WordCount += tokens.Count();
            context.TokenizedChunks.AddRange(tokens);
        }

        private async Task LoadHtmlDocument(ScrapeContext context)
        {
            var htmlDoc = new HtmlDocument();

            using (var response = await httpClient.GetAsync(context.Target))
            using (var responseStream = await response.Content.ReadAsStreamAsync())
            {
                // Probe response headers for correct encoding type and force HtmlDocument to use it if available
                var probedEncoding = response.Content.Headers?.ContentType?.CharSet;
                if (probedEncoding != null)
                {
                    Encoding resolvedEncoding = null;
                    try
                    {
                        resolvedEncoding = Encoding.GetEncoding(probedEncoding);
                    }
                    finally { } // OK not handle this because encoding can be inferred later by the parser from <meta />
                    htmlDoc.Load(responseStream, resolvedEncoding);
                }
                else
                {
                    htmlDoc.Load(responseStream);
                }
            }

            context.Document = htmlDoc;
        }
    }
}
