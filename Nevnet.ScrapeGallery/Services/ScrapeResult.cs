﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Nevnet.ScrapeGallery.Services
{
    public class ScrapeResult
    {
        public int TotalWords { get; set;  }

        public Tuple<string, int>[] Top10Words { get; set;  }

        public string[] Images { get; set;  }


        public TimeSpan TimeElapsed { get; set; }

        public ScrapeResult() { }
    }
}